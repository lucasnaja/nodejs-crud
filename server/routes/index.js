const Model = require('../models')
const bodyParser = require('body-parser')
const { _DATABASE : db } = require('../configs/global')

module.exports = async _server => {
	_server.use(bodyParser.json())

	const tables = await Model.selectTables()
		.then(response => response.map(item => item[`Tables_in_${db}`]))
		.catch(() => [])

	tables.forEach(table => {
		// CREATE
		_server.post(`/create/${table}`, (req, res) => {
			Model.create(table, req.body)
				.then(response => res.status(200).json(response))
				.catch(error => res.status(404).json(error))
		})

		// UPDATE
		_server.put(`/update/${table}`, (req, res) => {
			const names = Object.keys(req.body)
			let serialize = '{'

			for (let i = 0; i < names.length; i++) {
				if (names[i] !== '_id')
					serialize += `"${names[i]}":"${req.body[names[i]]}"${i === names.length - 1 ? '}' : ','}`
			}

			serialize = JSON.parse(serialize)

			Model.update(req.body._id, table, serialize)
				.then(response => res.status(200).json(response))
				.catch(error => res.status(404).json(error))
		})

		// SELECTS
		_server.get(`/select/${table}`, (req, res) => {
			Model.select(table)
				.then(response => res.status(200).json(response))
				.catch(error => res.status(404).json(error))
		})

		_server.get(`/select/${table}/:_id`, (req, res) => {
			Model.selectByID(req.params._id, table)
				.then(response => res.status(200).json(response))
				.catch(error => res.status(404).json(error))
		})

		// DELETE
		_server.delete(`/delete/${table}`, (req, res) => {
			Model.deleteByID(req.body._id, table)
				.then(response => res.status(200).send(response))
				.catch(error => res.status(404).json(error))
		})
	})
}
