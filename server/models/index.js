const mysql = require('../database')

class Model {
	async selectTables() {
		return await mysql.conn.query('SHOW TABLES')
	}

	async create(table, values) {
		let sql = ''
		const names = Object.keys(values)
		const length = names.length

		for (let i = 0; i < length; i++) {
			sql += `?${i !== length - 1 ? ',' : ''}`
		}

		return await mysql.conn.query(`INSERT INTO ${table} VALUES(DEFAULT, ${sql})`, names)
	}

	async update(_id, table, values) {
		let sql = ''
		const names = Object.keys(values)
		const length = names.length

		for (let i = 0; i < length; i++) {
			sql += `${names[i]}=?${i !== length - 1 ? ',' : ''}`
		}

		return await mysql.conn.query(`UPDATE ${table} SET ${sql} WHERE _id=?`, [...Object.values(values), _id])
	}

	async select(table) {
		return await mysql.conn.query(`SELECT * FROM ${table}`)
	}

	async selectByID(_id, table) {
		return await mysql.conn.query(`SELECT * FROM ${table} WHERE _id=?`, [_id])
	}

	async deleteByID(_id, table) {
		return await mysql.conn.query(`DELETE FROM ${table} WHERE _id=?`, [_id])
	}
}

module.exports = new Model()
