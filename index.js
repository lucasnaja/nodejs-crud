const server = require('express')()
const { _PORT: port } = require('./server/configs/global')

require('./server/database')
	.connect()
	.then(() => {
		require('./server/routes')(server)
			.then(() => {
				server.get('/', (req, res) => res.status(200).send(`<h1>http://localhost:${port}/select/table to see its content</h1>`))
				server.use((req, res) => res.status(200).send('<h1>This page doesn\'t exist.</h1>'))
			})

		server.listen(port, () => console.log(`Running on port ${port}.\nLink: http://localhost:${port}/`))
	})
